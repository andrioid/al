package al

import (
	"log"
	"time"

	//_ "github.com/go-sql-driver/mysql"
	gorm "github.com/jinzhu/gorm"
	_ "github.com/lib/pq" // postgres driver
	_ "github.com/mattn/go-sqlite3"
)

var db *gorm.DB

func automigrate() {
	if options.DbProvider == "postgres" {
		dbi, err := gorm.Open("postgres", options.DbOptions)
		if err != nil {
			panic(err)
		}
		db = &dbi
	} else {
		dbi, err := gorm.Open("sqlite3", "test.db")
		if err != nil {
			panic(err)
		}
		db = &dbi
	}

	if db == nil {
		panic("Database is nil")
	}

	if err := db.DB().Ping(); err != nil {
		panic(err)
	}
	db.AutoMigrate(&user{}, &role{}, &permission{}, &auth{}, &media{}, &tag{})
	log.Println("Database initialized.")
}

type user struct {
	ID        int64
	Name      string
	Email     string
	Photo     string
	Auths     []auth
	Roles     []role `gorm:"many2many:user_roles"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
	//Google_id    string
	//Microsoft_id string
	//Photo        string
}

type role struct {
	ID          int64
	Name        string
	Permissions []permission `gorm:"many2many:role_permissions"`
}

type permission struct {
	ID   int64
	Name string
}

type auth struct {
	ID       int64
	UserID   int64
	External string
	Network  string
}

type mediaType int

const (
	photo mediaType = iota
	video
)

type media struct {
	ID          int64
	Title       string
	Description string
	Path        string
	Type        mediaType
	Tags        []tag `gorm:"many2many:media_tags"`
	People      []user
	Owner       user
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   time.Time
}

type tag struct {
	ID   int64
	Name string
}
