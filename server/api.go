package al

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type mediaList struct {
	name   string
	medias *[]media
}

func apiError(reason string, w http.ResponseWriter) {
	fmt.Fprintf(w, "Error: %s", reason)
}

func mediaApi(w http.ResponseWriter, r *http.Request) {
	// TODO: Validation!
	//fmt.Println(r.FormValue("mediaId"), r.FormValue("mediaTitle"), r.FormValue("mediaDate"))

	//mediaId := r.FormValue("mediaId")

	ml := mediaList{}
	ml.medias = &[]media{}
	views := []photoView{}

	db.Find(ml.medias).Limit(10)
	for _, me := range *ml.medias {
		fmt.Println(me)
		currentView := &photoView{}

		currentView.Title = me.Title
		currentView.Preview = fmt.Sprintf("//s3preview-demo.herokuapp.com/%s?t=large", me.Path) // todo: un-hardcode
		currentView.Page = fmt.Sprintf("//s3preview-demo.herokuapp.com/%s?t=large", me.Path)    // todo: un-hardcode
		currentView.ID = fmt.Sprintf("%d", me.ID)
		views = append(views, *currentView)
		//ctx.Photos = append(ctx.Photos, currentView)
	}
	mediaj, _ := json.Marshal(views)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write(mediaj)

	fmt.Println(ml)
	/*
		switch r.Method {
		// New Media
		case "POST":
			fmt.Fprintf(w, "post %s", mediaId)
		// Updating (e.g. title edit)
		case "PUT":
			po, _ := photos.Get(mediaId)
			po.Title = r.FormValue("mediaTitle")
			photos.Save(&po)

			//fmt.Fprintf(w, "put %s", mediaId)
		case "GET":
			po, _ := photos.Get(mediaId)
			poj, _ := json.Marshal(po)
			w.Header().Set("Content-Type", "application/json; charset=utf-8")
			w.Write(poj)
			//fmt.Println(po)

		default:
			fmt.Fprintf(w, "undefined %s", r.Method)
		}
	*/
}
