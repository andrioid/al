package al

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
)

// Options are using by Serve to configure the application
type Options struct {
	Port               int
	DbProvider         string
	DbOptions          string
	AwsAccessKeyID     string
	AwsSecretAccessKey string
}

var sessionStore = sessions.NewCookieStore([]byte("al-bum"))

var options *Options

// Serve is the entrypoint of Al
func Serve(opt Options) error {
	options = &opt

	// DB setup
	automigrate()

	// Block storage

	if opt.AwsAccessKeyID == "" || opt.AwsSecretAccessKey == "" {
		log.Println("AWS Not configured. Add AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY to environment")
		os.Exit(1)
	}

	// HTTP setup
	r := mux.NewRouter()
	//r.HandleFunc("/*", homeHandler)
	r.HandleFunc("/api/media", mediaApi)
	r.HandleFunc("/upload", uploadHandler)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("../client/dist/")))
	http.Handle("/", r)
	err := http.ListenAndServe(fmt.Sprintf(":%d", opt.Port), nil)
	if err != nil {
		return err
	}
	return nil
}

type applicationContext struct {
	Moo         string
	CurrentUser user
	Photos      []*photoView
	//Configuration *Config
}

type photoView struct {
	ID      string `json:"id"`
	Title   string `json:"title"`
	Preview string `json:"preview"`
	Page    string `json:"page"`
	Link    string `json:"link"`
}
