package al

import (
	"testing"

	"github.com/jinzhu/gorm"
)

var dbb = getDB()

func getDB() *gorm.DB {
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic(err)
	}
	db.DB().Ping()
	db.AutoMigrate(&user{}, &role{}, &permission{}, &auth{}, &media{}, &tag{})
	return db.Debug()
}

func TestMedia(t *testing.T) {
	db := dbb
	user := user{Name: "Moo"}
	db.Save(&user)
}
