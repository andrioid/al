package al

import (
	"fmt"
	"io"
	"log"
	"math/rand"
	"mime"
	"net/http"
	"path/filepath"
	"time"

	uuid "github.com/pborman/uuid"
	"github.com/rlmcpherson/s3gof3r"
)

func randSeq(n int) string {
	rand.Seed(time.Now().Unix())
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func generateNewFilename(ext string) string {
	year, month, day := time.Now().Date()
	filename := uuid.New()
	return fmt.Sprintf("al/%04d/%02d/%02d/%s%s", year, month, day, filename, ext)
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	//fmt.Printf("Current url: %s\n", r.URL)
	switch r.Method {

	case "POST":
		reader, err := r.MultipartReader()

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		//copy each part to destination.
		for {
			part, err := reader.NextPart()
			if err == io.EOF {
				break
			}

			//if part.FileName() is empty, skip this iteration.
			if part.FileName() == "" {
				continue
			}

			filename := generateNewFilename(filepath.Ext(part.FileName()))

			mimetype := mime.TypeByExtension(filepath.Ext(part.FileName()))
			s3head := http.Header{}
			s3head.Add("Content-Type", mimetype)

			log.Printf("New filename: %s - Old: %s\n", filename, part.FileName())

			//dst, err := storage.File(filename)
			//defer dst.Close()

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			k, err := s3gof3r.EnvKeys() // get S3 keys from environment
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			// Open bucket to put file into
			s3 := s3gof3r.New("", k)
			b := s3.Bucket("andridk-assets")

			// Open a PutWriter for upload
			wr, err := b.PutWriter(filename, s3head, nil)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			if _, err = io.Copy(wr, part); err != nil { // Copy into S3
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			if err = wr.Close(); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}

			/*
				if _, err := io.Copy(dst, part); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			*/
			mup := &media{
				Path: filename,
			}

			db.Save(&mup)
			// todo: add to database

			// File uploaded to store, let's add it to the index
		}

		http.Redirect(w, r, ".", http.StatusSeeOther)
		fmt.Fprintf(w, "Please go to the front-page. You miss the redirect.")

	default:
		return

	}
}
