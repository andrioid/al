package main

import (
	"os"
	"strconv"

	"gitlab.com/andri80/al/server"
)

func main() {
	opt := al.Options{}
	opt.Port = 8080
	opt.DbProvider = "sqlite"
	opt.DbOptions = "test.db"

	if os.Getenv("PORT") != "" {
		opt.Port, _ = strconv.Atoi(os.Getenv("PORT"))
	}

	if os.Getenv("DATABASE_TYPE") != "" {
		opt.DbProvider = os.Getenv("DATABASE_TYPE")
	}

	if os.Getenv("DATABASE_URL") != "" {
		opt.DbOptions = os.Getenv("DATABASE_URL")
	}

	if os.Getenv("AWS_ACCESS_KEY_ID") != "" {
		opt.AwsAccessKeyID = os.Getenv("AWS_ACCESS_KEY_ID")
	}

	if os.Getenv("AWS_SECRET_ACCESS_KEY") != "" {
		opt.AwsSecretAccessKey = os.Getenv("AWS_SECRET_ACCESS_KEY")
	}

	err := al.Serve(opt)
	if err != nil {
		panic(err)
	}
	//http.ListenAndServe(":80", nil) // listen for connections at port 9999 on the local machine
}
