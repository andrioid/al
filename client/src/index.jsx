import React from 'react'
import ReactDOM from 'react-dom'
import Router, {Route, IndexRoute} from 'react-router'
import App from './components/App'
import {MediaIndex} from './components/MediaIndex'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware, compose} from 'redux'
import reducer from './reducer'
import Login from './components/Login'
import MediaPage from './components/MediaPage'
import thunk from 'redux-thunk'
import createBrowserHistory from 'history/lib/createBrowserHistory'

const finalstore = compose(
	applyMiddleware(thunk),
	window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore)

const history = createBrowserHistory()

const store = finalstore(reducer)

ReactDOM.render(
	<Provider store={store}>
		<Router history={history}>
			<Route path='/' component={App}>
				<IndexRoute component={MediaIndex} />
				<Route path='login' component={Login} />
				<Route path='media/:mediaId' component={MediaPage} />
			</Route>
		</Router>
	</Provider>,
	document.getElementById('app')
)
