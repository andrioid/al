import axios from 'axios'

export function setState (state) {
	return {
		type: 'SET_STATE',
		state
	}
}

export function getMediaList () {
	return {
		meta: {remote: true},
		type: 'GET_MEDIA_LIST'
	}
}

export function SET_PREVIEW_SIZE (size) {
	return {
		type: 'SET_PREVIEW_SIZE',
		size: size
	}
}

function MEDIA_LIST_RECEIVED (json) {
	return {
		type: 'MEDIA_LIST_RECEIVED',
		payload: { items: json }
	}
}

export function fetchMediaList () {
	return function (dispatch) {
		axios.get('/api/media')
		.then(function (response) {
			dispatch(MEDIA_LIST_RECEIVED(response.data))
		})
		.catch(function (response) {
			console.log('api err', response)
		})
	}
}

export function commentClick (mediaId) {
	return {
		type: 'COMMENT_CLICKED',
		payload: { id: mediaId }
	}
}

export function likeClick (mediaId) {
	return {
		type: 'LIKE_CLICKED',
		payload: { id: mediaId }
	}
}

export function thunkTest () {
	return dispatch => {
		setTimeout(() => {
			// Yay! Can invoke sync or async actions with `dispatch`
			dispatch(getMediaList())
		}, 1000)
	}
}
