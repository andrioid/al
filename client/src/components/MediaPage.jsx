import React, { PropTypes } from 'react'
import {connect} from 'react-redux'
// import BaseLayout from '../layouts/base.jsx'

function mapStateToProps (state) {
	return {
		media: state.media
	}
}

class MediaPage extends React.Component {
	static propTypes () {
		return {
			params: PropTypes.object.isRequired,
			media: PropTypes.object.isRequired
		}
	}
	render () {
		console.log('mediaPage', this.props)
		let { params, media } = this.props
		let id = params.mediaId
		console.log('id', id)
		let mediaItem = media.get('items').find(function (obj) {
			return obj.get('id') === id
		})
		console.log('item', mediaItem.toJS())
		return (
			<div className='ui grid stackable'>
				<div className='twelve wide column center aligned' sstyle={{'border': '1px solid #bb0000'}}>
					<div className='image fluid'>
						<img className='image fluid' height='90%' style={{'maxWidth': '100%'}} src={mediaItem.get('page')} />
					</div>
					<div className='description'>
					Lorem Ipsum came to the shop today and ate all my Lorem Bacon.
					</div>
				</div>
				<div className='four wide column' style={{background: '#ffffff'}}>
					<div className='ui comments'>
						<h3 className='ui dividing header'>Comments</h3>
						<div className='comment'>
							<a className='avatar'>
								<img src='//lorempixum.com/100/100/cats' />
							</a>
							<div className='content'>
								<a className='author'>Matt</a>
								<div className='metadata'>
									<span className='date'>Today at 16:23</span>
								</div>
								<div className='text'>
									How artistic!
								</div>
								<div className='actions'>
									<a className='reply'>Reply</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default connect(mapStateToProps)(MediaPage)
