// Purpose: Show current user, or link to a login page.
// - Input: user state
import React from 'react'
import { connect } from 'react-redux'

function mapStateToProps (state) {
	return {
		user: state.user
	}
}

class UserIndicator extends React.Component {
	getUser () {
		if (this.props.user) {
			return (
				<div className='ui simple dropdown item'>
					{this.props.user.name}
					<i className='dropdown icon'/>
					<div className='menu'>
						<div className='item'><a href='#' onClick={this.props.logout}>Log out</a></div>
					</div>
				</div>)
		} else {
			return (<li className='dropdown'>
						<a href='#' className='dropdown-toggle' data-toggle='dropdown'>Guest<span className='caret'></span></a>
						<ul className='dropdown-menu' role='menu'>
							<li><a href='#' onClick={this.props.login}>Log in</a></li>
						</ul>
						</li>)
		}
	}
	render () {
		var user = this.getUser()
		return user || false
	}
}

export default connect(mapStateToProps)(UserIndicator)
