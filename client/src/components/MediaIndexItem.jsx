import React, { PropTypes } from 'react'
import { Link } from 'react-router'

export default class MediaIndexItem extends React.Component {
	static propTypes () {
		return {
			mediaItem: PropTypes.object.isRequired,
			likeClick: PropTypes.func.isRequired,
			commentClick: PropTypes.func.isRequired
		}
	}
	render () {
		console.log('mit', this.props)
		let { mediaItem, likeClick, commentClick } = this.props
		let miv = mediaItem
		if (!likeClick || !commentClick) {
			console.log('wwaaat', this.props)
		}
		return (
			<div key={miv.get('id')} className='column' sstyle={{border: '1px solid #bb0000'}}>
				<div className='ui fluid card'>
					<div className='image'>
						<Link to={`/media/${miv.get('id')}`} state={{ media: miv, modal: true }}>
								<img className='ui fluid image' src={miv.get('preview')} title={miv.get('title')} />
						</Link>
					</div>
					<div className='content'>
						<div className='description'>
							Lorem Ipsum came to the store today and ate all my Lorem Bacon.
						</div>
					</div>
					<div className='extra content'>
						<span className='right floated'>
							<a onClick={this.props.commentClick.bind(null, miv.get('id'))}>
								<i className='comments icon'/>
								1 comment
							</a>
						</span>
						<span>
							<a href='#' onClick={this.props.likeClick.bind(null, miv.get('id'))}>
								<i className='heart icon'/>
								5 likes
							</a>
						</span>
					</div>
				</div>
			</div>
		)
	}
}
