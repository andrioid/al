import React from 'react'
import Mousetrap from 'mousetrap'

function close () {
	history.back()
}

export default class Modal extends React.Component {
	componentDidMount () {
		Mousetrap.bind(['esc'], function () {
			close()
		})
	}
	componentWillUnmount () {
		Mousetrap.unbind('esc', function () {
			close()
		})
	}
	render () {
		let { props } = this
		// let display = (props.isOpen === true) ? 'block' : 'none'
		return (
		<div className='ui dimmer active visible' style={{zIndex: 1000}}>
			<div className='ui fullscreen basic modal active' style={{top: '3%'}}>
				<i onClick={close} className='close icon'></i>
				<div className='image content'>
				{props.children}
				</div>
				<div className='actions'>
					<div onClick={close} className='ui black cancel button'>
					Close
					</div>
				</div>

			</div>
		</div>
		)
	}
}
