// Purpose: Show current user, or link to a login page.
// - Input: user state
import React, { PropTypes } from 'react'
// import UserIndicator from './UserIndicator'
import {connect} from 'react-redux'
import BaseLayout from '../layouts/base.jsx'
import * as actions from '../actions.js'
import MediaIndexItem from './MediaIndexItem.jsx'
import Immutable from 'immutable'

function mapStateToProps (state) {
	return {
		state: state,
		media: state.media
	}
}

export class DefaultView extends React.Component {

	constructor (props) {
		super(props)
		// this.renderMediaItems = this.renderMediaItems.bind(this)
	}
	static propTypes () {
		return {
			media: PropTypes.object.isRequired,
			fetchMediaList: PropTypes.func.isRequired,
			thunkTest: PropTypes.func.isOptional,
			likeClick: PropTypes.func.isRequired,
			commentClick: PropTypes.func.isRequired
		}
	}
	componentWillMount () {
		this.props.fetchMediaList()
	}
	render () {
		console.log('mi props', this.props)
		let { media, likeClick, commentClick } = this.props
		let items = media.get('items') || Immutable.fromJS({})
		return (
				<BaseLayout>
					<div className='ui five column grid doubling stackable' style={{margin: '0.5em'}}>
						{
							items.valueSeq().map(function (miv, mi) {
								return (
									<MediaIndexItem key={mi} mediaItem={miv} likeClick={likeClick} commentClick={commentClick} />
								)
							})
						}
					</div>
				</BaseLayout>
			)
	}
}

export const MediaIndex = connect(mapStateToProps, actions)(DefaultView)
