import React, { PropTypes } from 'react'
import {connect} from 'react-redux'
import Modal from './Modal.jsx'

function mapStateToProps (state) {
	return {
		state
	}
}

class App extends React.Component {
	static propTypes () {
		return {
			children: PropTypes.object.isRequired,
			location: PropTypes.object.isRequired
		}
	}
	componentWillReceiveProps (nextProps) {
		// if we changed routes...
		if ((
			nextProps.location.key !== this.props.location.key &&
			nextProps.location.state &&
			nextProps.location.state.modal
		)) {
			// save the old children (just like animation)
			this.previousChildren = this.props.children
		}
	}
	render () {
		let { location } = this.props
		let isModal = (
			location.state &&
			location.state.modal &&
			this.previousChildren
		)

		return (
			<div>
				{ isModal
					? this.previousChildren
					: this.props.children
				}

				{isModal && (
					<Modal isOpen={true}>
						{this.props.children}
					</Modal>
				)}
			</div>

		)
	}
}

export default connect(mapStateToProps)(App)
