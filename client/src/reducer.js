import Immutable from 'immutable'
import { combineReducers } from 'redux'

function mediaStore (state = Immutable.fromJS({}), action) {
	switch (action.type) {
	case 'MEDIA_LIST_RECEIVED':
		console.log('updating media state')
		return state.set('items', Immutable.fromJS(action.payload.items))
	case 'MEDIA_ITEM_LIKE_CLICKED':
		return state
	case 'MEDIA_ITEM_COMMENT_CLICKED':
		return state
	default:
		return state
	}
	return state
}

export default combineReducers({
	media: mediaStore
})
