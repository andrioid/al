// Purpose: Show current user, or link to a login page.
// - Input: user state
import React, { PropTypes } from 'react'
import UserIndicator from '../components/UserIndicator'
import { Link } from 'react-router'

export default class BaseLayout extends React.Component {
	static propTypes () {
		return {
			children: PropTypes.object.isRequired
		}
	}
	render () {
		return (<div className='appWrapper'>
				<div className='ui inverted segment'>
				<div className='ui secondary pointing menu inverted'>
						<a href='#' className='header item active'>Al-Bum</a>
						<div className='right menu'>
								<div className='item'><a href='#'>Upload</a></div>
								<UserIndicator/>
						</div>
				</div>
				</div>
				<div className='ui container fluid'>
						{this.props.children}
						<footer>
								<p>Made with <i className='heart icon' style={{color: '#bb0000'}}></i> in Denmark.</p>
						</footer>
				</div>
		</div>)
	}
}
