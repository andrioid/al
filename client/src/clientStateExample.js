var state = {
  "user": {
    name: "Andri Óskarsson",
    context: {
      "previewSize": "large"
    }
  },
  "media": {
    "page": 1,
    "items": [
      {
        id: 1,
        preview: "//s3preview-demo.herokuapp.com/familyblog/2014/11/20/b5fbee0f-0396-4468-88cf-a94e7f69f898.JPG?t=large",
      },
      {
        id: 2,
        preview: "//s3preview-demo.herokuapp.com/familyblog/2014/12/25/7462ad41-f8c5-4f30-b673-cdabdf956807.jpg?t=large"
      },
      {
        id: 3,
        preview: "//s3preview-demo.herokuapp.com/familyblog/2014/12/25/7462ad41-f8c5-4f30-b673-cdabdf956807.jpg?t=large"
      },
      {
        id: 4,
        preview: "//s3preview-demo.herokuapp.com/familyblog/2014/12/25/7462ad41-f8c5-4f30-b673-cdabdf956807.jpg?t=large"
      },
      {
        id: 5,
        preview: "//s3preview-demo.herokuapp.com/familyblog/2014/12/25/7462ad41-f8c5-4f30-b673-cdabdf956807.jpg?t=large"
      }
    ]
  },
  "notifications": ["hello world"]
};

var stateGuest = {
  "user": null,
  "media": {
    "page": 1,
    "items": []
  },
  "notifications": ["hello world"]
};

export default state;
