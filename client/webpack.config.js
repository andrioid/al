var webpack = require('webpack')
var process = require('process')

var plugins = [
	new webpack.HotModuleReplacementPlugin()
]

var entry = [
	'./src/index.jsx'
]

if (process.env && process.env.NODE_ENV === 'production') {
	plugins = []
} else {
	entry.push('webpack-dev-server/client?http://localhost:8080')
	entry.push('webpack/hot/only-dev-server')
}

module.exports = {
	entry,
	module: {
		loaders: [{
			test: /\.jsx?$/,
			exclude: /node_modules/,
			loader: 'babel'
		}]
	},
	resolve: {
		extensions: ['', '.js', '.jsx']
	},
	output: {
		path: __dirname + '/dist',
		publicPath: '/',
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: './dist',
		historyApiFallback: true
	},
	plugins
}
