
// TODO: Top bar, with upload and UserIndicator
// TODO: Load X media and display pager
// TODO: If user is guest and no media displayed, pop up login modal

// Query options
// - Latest (defaults to timeline)
// - People (

// Í staðinn fyrir albums, væri hægt að nota lista. Þannig myndi maður velja myndir og svo setja þær í lista sem maður gefur nafn.
// Það workflow passar miklu betur inn í að maður geti bara fleygt inn myndum og flokkað þær seinna.

// Tags:
// Sjálfvirkt setja inn ár, ár-mánuð og ár-mánuð-dag tag á myndir útfrá taken-date (ef taken-date finnst ekki, nota upload date)

// Lists:
// Ef maður er með eitthvað í listanum sínum þá opnast sidebar með litlum thumbs af því sem er í listanum. Möguleiki á að endurnefna og deila.
// Ef maður er að skoða lista, þá leggja til undir lista input field í upload dialog að bæta við á listann. It's all about context baby.
// List hirarchy? Hátíðir -> Jól, Páskar, Jól 2015, Páskar 2016

// Media (í listview)
// - Edit takki
// - Add to list takki
// - <3

// Media Sér View
// - Stór mynd
// - Sýna fyrirsögn og texta, ef einhver
// - Sýna öll tög
// - Linka í lista (ef réttindi leyfa) sem nota myndina

// Views
// TODO: Collage (current default). Mix images to fill the page
// TODO: Timeline. Show month, year. Then images in rows.